using System;
using Xunit;

namespace ApplicationTests
{
    public class UnitTest1
    {
        [Fact]
        public void Add_AddTwoNumbers_ShouldReturnSum()
        {
            //Arrange
            Calculator.Calc myCalc = new Calculator.Calc();
            int lhs = 1;
            int rhs = 1;
            int expected = lhs + rhs;
            //act
            int actual = myCalc.Add(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Subtract_SubtractTwoNumbers_ShouldReturnSum()
        {
            //Arrange
            Calculator.Calc myCalc = new Calculator.Calc();
            int lhs = 5;
            int rhs = 2;
            int expected = lhs - rhs;
            //act
            int actual = myCalc.Subtract(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Multiply_MultiplyTwoNumbers_ShouldReturnSum()
        {
            //Arrange
            Calculator.Calc myCalc = new Calculator.Calc();
            int lhs = 3;
            int rhs = 3;
            int expected = lhs * rhs;
            //act
            int actual = myCalc.Multiply(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Divide_DivideTwoNumbers_ShouldReturnSum()
        {
            //Arrange
            Calculator.Calc myCalc = new Calculator.Calc();
            int lhs = 10;
            int rhs = 2;
            double expected = lhs / rhs;
            //act
            double actual = myCalc.Divide(lhs, rhs);
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Divide_DivideByZero_ShouldThrowDivideByZeroException()
        {
            //Arrange
            Calculator.Calc myCalc = new Calculator.Calc();
            int lhs = 10;
            int rhs = 0;
            //Act and Assert
            Assert.Throws<DivideByZeroException>(() => myCalc.Divide(lhs, rhs));
        }
    }
}
