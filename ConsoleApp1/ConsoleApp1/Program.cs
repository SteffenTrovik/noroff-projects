﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        public static int getUserNumber()
        // static method to get user input and validate 
        {
            int square = -1;
            bool positive = false;
            bool number = false;
            Console.WriteLine("How big of a square would you like?");
            while (!(positive && number))   // loop until we have an integer that is positive
            {
                number = int.TryParse(Console.ReadLine(), out square);
                if (square > 0)
                {
                    break;
                }
                Console.WriteLine("Invalid input, must be a non-negative whole number");
            }
            return square;
        }

        static void Main(string[] args)
        {
            int square = getUserNumber();
            for (int i = 0; i < square; i++)
            {
                for (int j = 0; j < square; j++)
                {
                    if (j == 0 || j == square - 1 || i == 0 || i == square - 1) //only add stars on the edge cases
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        if (square > 4)
                        {
                            if (i == 1 || i == square - 2 || j == 1 || j == square - 2)

                            {
                                Console.Write(" ");
                            }
                            else
                            {
                                Console.Write("*"); // add spaces in the center
                            }
                        }
                        else
                        {
                            Console.Write(" "); // add spaces in the center
                        }
                    }
                    Console.WriteLine(""); // new line after every set of rows
                }
            }
        }
    }
}