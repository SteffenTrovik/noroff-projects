﻿using System;

namespace Fizzbuzz
{
    class Program
    {

        public static string getFizzBuzz(int input)
        {
            if (input % 3 == 0 && input%5==0)
            {
                return "FizzBuzz";
            }else if (input % 3 == 0)
            {
                return "Fizz";
            }
            else if (input % 5 == 0)
            {
                return "Buzz";
            }else
            return ""+input;
        }


        static void Main(string[] args)
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(getFizzBuzz(i));
            }
        }
    }
}
