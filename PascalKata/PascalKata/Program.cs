﻿using System;
using System.Text;

namespace PascalKata
{
    class Program
    {
        static public String PascalCaseConverter(string input)
        {
            StringBuilder sb = new StringBuilder();
            Boolean capsulate = true;
            char nextChar;
            foreach (char c in input)
            {
                if (!char.IsLetter(c))
                {
                    capsulate = true;
                }
                else
                {
                    if (capsulate)
                    {
                        nextChar = char.ToUpper(c);
                        capsulate = false;
                    }
                    else
                    {
                        nextChar = char.ToLower(c);
                    }
                    sb.Append(nextChar);
                }
            }
            return sb.ToString();
        }

        static void Main(string[] args)
        {
            Console.WriteLine(PascalCaseConverter("The qUick! bRoWn fox  jumped, OVER the   lazy. dog"));
        }
    }
}
