﻿using System;

namespace tryitoutday2 { 
{
    public abstract class Animal
    {
        public enum Biome { Dessert, Ocean, Jungle };

        private string name;
        private double weight;
        priate Biome home;


        public string Name { get => name; set => name = value; }
        public double Weight { get => weight; set => weight = value; }
        public Biome home{ get => home; set => home = value; }


        public Animal(string name, double weight)
        {
            this.Name = name;
            this.weight = weight;
        }

        public string Present(Boolean print)
        {
            string presentString = "This animal is called " + this.name + ", it weighhs " + this.weight + " and it resides in the " + home;
            if (print) {
                Console.WriteLine(presentString);
            }
            return presentString;
        }

        public abstract void MakeNoise();

        public virtual void Sleep()
        {
            Console.WriteLine("Animal is sleeping.");
        }
    }
}
}