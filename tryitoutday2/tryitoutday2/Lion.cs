﻿using System;

namespace tryitoutday2
{

    public class Lion : Animal, IPredator, IClimber
    {
        public Lion(string name, double weight) : base(name, weight) {
            this.home = Biome.Jungle;
        }


        public override void MakeNoise()
        {
            Console.WriteLine("LionRaaawr");
        }

        public override void Sleep()
        {
            Console.WriteLine("ZZZZZZZZZ");
        }

        public void Prey()
        {
            Console.WriteLine("The Lion runs after his prey at menacing speeds");
        }

        public void Climb()
        {
            Console.WriteLine("The Lion climbs");
        }
    }
}