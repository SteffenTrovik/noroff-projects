﻿using System;

namespace tryitoutday2
{
    class Animal1
    {
        private string name;
        private double weight;
        private string home;

        public string Name { get => name; set => name = value; }
        public double Weight { get => weight; set => weight = value; }
        public string Home { get => home; set => home = value; }


        public Animal1(string name, double weight, string home)
        {
            this.Name = name;
            this.weight = weight;
            this.Home = home;
        }

        public string Present(Boolean print)
        {
            string presentString = "This animal is called " + this.name + ", it weighhs " + this.weight + " and it resides in the " + home;
            if (print) {
                Console.WriteLine(presentString);
            }
            return presentString;
        }

        public void MakeNoise()
        {
            Console.WriteLine("rawr");
        }
    }
}
