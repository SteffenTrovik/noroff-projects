﻿using System;

namespace tryitoutday2
{

    public class Shark : Animal, IPredator
    {
        public Shark(string name, double weight) : base(name, weight) {
            this.home = Biome.Ocean;
        }
        public int length { get; set; }


        public Shark(string name, double weight,int length) : base(name, weight)
        {
            this.home = Biome.Ocean;
            this.length = length;
        }

        public override void MakeNoise()
        {
            Console.WriteLine("SharkRaaawr");
        }

        public override void Sleep()
        {
            Console.WriteLine("ZBlubBlubZBlubBlubZZ");
        }

        public void Prey()
        {
            Console.WriteLine("The shark swims after its prey with scary backgroundmusic");
        }


    }
}
